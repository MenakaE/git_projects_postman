# API_Project_Postman_Tool


**About project**

Postman is a tool for **SOAP and REST API testing** of different HTTP methods. This project is designed to trigger and validate API using Postman and Newman and to generate reports.

My project consists of different components

1. Test Driving Mechanism
2. Collection files
3. Environment files
4. Data files
5. Reports

**1. Test Driving Mechanism**

- **Postman** - A **desktop application** where it allows to automate API test by executing multiple requests in a collection through **Collection Runner**.
- **Newman** - A **command line** collection runner for Postman, that enables to test the Postman collection directly without Postman.

**2. Collection files**

- **Different Http Methods**

1. Post - To create a new resource
1. Patch - To update a specific existing resource
1. Put - To replace an entire existing resource 
1. Get - To retrieve data on a server
1. Delete - To remove data from the database

- **Colections**

    Allows to organize and execute multiple API as a single test suite.


- **TestScripts**

1. Having separate testscripts for each HTTP methods using **Postman library** in JavaScript language, to extract and parse rseponse and request and for validation of API
2. Allows for **request chaining** to define the flow of API execution 
3. When requests are failed due to timeout or error in environment, **Auto retry** of API is designed for fixed number of iteration using **setNextRequest()** function

- **Request body**

    Contains the request payload of each API in Json format for REST API and xml format for SOAP API.

- **Pre-request Script**

    Allows to perform action before sending a request, like setting up dynamic data
 
- **Endpoint**

    Allows to send the endpoint of each methods, by specifying the method to trigger API


**3. Environment files**

Set of environment variables to use in sending request. It has endpoints of all API, with hostname as Base url and specific resources for each API 

**4. Data files**

Consists of test data, which are reading from an excel file for request payload to test API 

**5. Reports**
- **Html report** - Generates the html report while executing by newman commands i.e -r html, contains the result of test execution.
- **Htmlextra report** - Generates the htmlextra report while executing by newman commands i.e -r htmlextra, contains detailed representation of test execution result.


**Added files**


Cloned into GitLab account using https link and Added collection, environment files, datafile and report files using the command line from GitBash with the following commands:

```
git clone [https_link]
git init
git checkout -b "Menaka_First_Checkin"
git add *
git status
git commit -m "Adding files into repository"
git push origin Menaka_First_Checkin

```


